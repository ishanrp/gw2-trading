package controllers

import javax.inject.Inject

import org.joda.time.DateTime

import scala.util.{Success, Failure}
import models._
import play.api._
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.libs.ws._
import play.api.mvc._
import ws.GW2ws

import scala.concurrent.Future

class Application @Inject() (ws: WSClient) extends Controller {
  implicit val context = play.api.libs.concurrent.Execution.Implicits.defaultContext

  implicit val dateReads = Reads.jodaDateReads("yyyy-MM-dd'T'HH:mm:ssZ")
  implicit val itemsReads: Reads[Item] = (
    (__ \ "id").read[Long] and
    (__ \ "name").read[String] and
    (__ \ "type").read[String] and
    (__ \ "description").readNullable[String] and
    (__ \ "icon").read[String] and
    (__ \ "level").read[Int] and
    (__ \ "vendor_value").read[Long] and
    (__ \ "rarity").read[String]
  )(Item.apply _)

  implicit val itemsOrderWriter: Writes[ItemOrder] = Json.writes[ItemOrder]

  def index = Action {
    Ok(views.html.index("Your new application is ready."))
  }

  def getOrders(fulfilled: String, orderType: String) = Action { implicit request =>
    if (Seq("history", "current").contains(fulfilled) && Seq("buys", "sells").contains(orderType)) {
      val isCurrent = if (fulfilled == "current") true else false
      val isBuy = if (orderType == "buys") true else false
      Ok(Json.toJson(DB.query[ItemOrder].whereEqual("isCurrent", isCurrent).whereEqual("isBuy", isBuy).fetch()))
    } else NotFound
  }

  def refreshOrders(fulfilled: String, orderType: String) = Action.async { implicit request =>
    if (Seq("history", "current").contains(fulfilled) && Seq("buys", "sells").contains(orderType)) {
      val isCurrent = if (fulfilled == "current") true else false
      val isBuy = if (orderType == "buys") true else false

      implicit val itemsOrderReads: Reads[ItemOrder] = (
        (__ \ "id").read[Long] and
          (__ \ "item_id").read[Long] and
          (__ \ "price").read[Long] and
          (__ \ "quantity").read[Long] and
          (__ \ "created").read[DateTime] and
          (__ \ "purchased").readNullable[DateTime] and
          (__ \ "isCurrent").read[Boolean].orElse(Reads.pure(isCurrent)) and
          (__ \ "isBuy").read[Boolean].orElse(Reads.pure(isBuy))
        )(ItemOrder.apply _)

      ws.url(GW2ws.tradeUri + fulfilled + "/" + orderType).withHeaders("Authorization" -> "Bearer 73717FC8-4F0C-BF4F-93D4-3DCEEDE0411E59A2B4FD-13E2-4E2D-8687-166461B9A3C3").get().map{
        response =>
          val items = response.json.as[List[ItemOrder]]
          items.foreach{i =>
            try {
              DB.save(i)
            }
            catch {
              case e: Exception => None
            }
          }
          Ok(Json.toJson(items))
      }
    } else Future (NotFound)
  }

  def items = Action { implicit request =>
    val futureResult: Future[JsArray] = ws.url(GW2ws.itemsURL).get().map{
      response =>
        response.json.as[JsArray]
    }

    futureResult.onComplete{
      case Success(itemIds) =>
        println(itemIds.value.size)
        itemIds.value.grouped(100).map(ids => ids.mkString(",")).foreach(ids => {
          val futureItems = ws.url(GW2ws.itemsURL + "?ids=" + ids).get().map{
            response => response.json.as[List[Item]]
          }

          futureItems.onComplete {
            case Success(items) => items.foreach(DB.save)
            case Failure(error) => println(error)
          }
        })
      case Failure(error) => println(error)
    }
    Ok(views.html.index("Done"))
  }

  def itemCount = Action {
    Ok(Json.toJson(DB.query[Item].fetch()))
  }
}
