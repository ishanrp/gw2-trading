package ws

import javax.inject.Inject
import scala.concurrent.Future

import play.api.mvc._
import play.api.libs.ws._

/**
 * Created by ishan on 08-Aug-15.
 */

object GW2ws {
  val uri = "https://api.guildwars2.com/"
  val version = "v2/"
  val baseUri = uri + version

  val tradeUri = baseUri + "commerce/transactions/"
  val itemsURL = baseUri + "items"
  val currentBuys = tradeUri + "current/buys"
  val historyBuys = tradeUri + "history/buys"
}

case class GW2ws @Inject() (uri: String, ws: WSClient) {

}
