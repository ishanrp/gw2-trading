import java.time.format.DateTimeFormatter
import play.api.libs.json._
import play.api.libs.functional.syntax._
import org.joda.time.DateTime
import play.api.libs.json.{Json, Reads}
implicit val dateReads = Reads.jodaDateReads("yyyy-MM-dd'T'HH:mm:ssZ")
case class UserInfo(userName: String, startDate: DateTime)
implicit val userInfoReads = Json.reads[UserInfo]
var json = Json.parse("""{"userName": "joeuser","startDate": "2015-08-06T09:14:26+00:00" }""")
println(json)
val json2 = json.as[JsObject] ++ Json.obj("isBuy" -> true)
println(json2)
