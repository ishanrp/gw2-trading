package models

import play.api.libs.json.Json

/**
 * Created by ishan on 08-Aug-15.
 */
case class Item(itemId: Long, name: String, itemType:String, description: Option[String], icon: String, level: Int, vendorValue: Long, rarity: String)

object Item {
  implicit val itemFormat = Json.format[Item]
}