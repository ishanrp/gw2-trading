package models

import org.joda.time.DateTime

/**
 * Created by ishan on 08-Aug-15.
 */
case class ItemOrder(orderId: Long, itemId: Long, price: Long, quantity: Long, created: DateTime, purchased: Option[DateTime], isCurrent: Boolean, isBuy: Boolean)
