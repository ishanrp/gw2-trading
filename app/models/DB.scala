package models

import sorm._

/**
 * Created by ishan on 08-Aug-15.
 */
object DB extends Instance(entities = Seq(Entity[Item](unique = Set() + Seq("itemId")), Entity[ItemOrder](unique = Set() + Seq("orderId"))),
  url = "jdbc:mysql://localhost:3306/gw2trader?useUnicode=true&characterEncoding=UTF-8", user = "root", password = "root")
